# Sprint Math - An Android App for learning quick math.

This project is a part of the course 44644-Mobile Computing - Android, at Northwest for Spring 2018 semester. This is being implemented by the group 6 called Trial Blazers, of that course. This group has 4 develpors in total.

## Team:
Trial Blazers 
1) Nilantha Dambadeni Kalu Achchillage
2) Vineeth Agarwal
3) Ujjawal Kumar 
4) Vishal Chilka


## Project Source Links
Repository: https://bitbucket.org/android_app_sprintmath_team/sprint-math-android-app.git

## Application information
### Goal of project
In this quickly growing world, the children are getting smarter and smarter. However,
there are some conventional factors like speed of thinking of a child that makes him/her
smartest of the smart. Sprint Math is a way to make children sharper and smarter by learning
Math quickly. However, despite providing Chrome Book to students the current approach to
learn sprint math in schools is paper based. We can take examples of Eugene Field Elementary
School in Nodaway county or the Horace Mann Laboratory School at Northwest. We wanted to take an opportunity to contribute to the growth of these schools by introducing an app for
Sprint Math.

### Application Functionality and flow:
* The Application will have main screen which has three buttons.Easy, Moderate and Hard. 
* These buttons specify the difficulty level of Sprint Math.
* Clicking on any button will start a new activity with the appropriate difficulty level.
* DialogBox will be thrown on reaching highest score for level. 
* Level Sumary can be checked and also cleared off if necessary.

#### Easy level activity
It will have Math problems focusing Grade 1 & Grade 2 Students. 
It will be limited to 20 questions. Once a user completes answering those 20 questions, 
he/she will be taken to the summary page where user can see the scores achieved.  
Also they can see the questions highlighted in red, for which user selected a wrong answer. 
There will be an option to restart the module or switch to a different module on summary page.

#### Moderate level activity
It will have Math problems aiming at Grade 3 & Grade 4 Students. This is a 60 seconds time limited module. 
This module includes Math problems with arithmetic operations of substraction & addition for numbers between 0 to 20. 
Once a user spends 60 seconds, he/she will be taken to the summary page where user can see the scores achieved. 
Also they can see the questions highlighted in red, for which user selected a wrong answer. 
There will be an option to restart the module or switch to a different module on summary page.

#### Hard level activity 
It will have Math problems focusing Grade 5 Students.  This is a 60 seconds time limited module. 
This module includes Math problems with arithmetic operations of substraction & addition for numbers between 0 to 40. 
Once a user spends 60 seconds, he/she will be taken to the summary page where user can see the scores achieved. 
Also they can see the questions highlighted in red, for which user selected a wrong answer. 
There will be an option to restart the module or switch to a different module on summary page.

## Application Technical Specifications
### Test credentials: 
None, since no login is required.

### Sequence information/ Steps to be followed to test the application:
* Launch SprintMath App--> Click on Start Button--> Click on the module you want to use among Easy or Moderate or Hard 
* Read question and click on correct answer which is one among 4 options, to earn points
* Once the level is complete Easy-20 questions, Moderate or Hard (completion of 60 seconds), veiw scores and wrong answers at auto directed summary page
* Click on Level Summary to check summary. Clear History if you wish. 
* Click on Change module to change difficulty level or Click Restart Module to start the same difficulty level again.
* Repeat the process as you like.

### APK: 
sprint-math-android-app\app\build\outputs\apk\debug

### Supported devices:
It is compatible with almost all android smart phones, tablets, GenyMotion, Emulator, having Android version 4.4 or above.

### Responsibilities:
* Nilantha: Created and developed most of the layouts. Also wrote functionality for Easy.java module. Introduced the time limit logic in Moderate.java and Hard.java modules.
* Vishal: Designed all logos and images used in project. He created the Moderate.java layout and worked on functionalities to display data in Summary.java layout.
* Vineeth: Worked on major Moderate.java functionality and layout. Also created layout for Summary.
* Ujjawal: Worked on Hard.java functionality other than timing. Also worked on switching between activities.