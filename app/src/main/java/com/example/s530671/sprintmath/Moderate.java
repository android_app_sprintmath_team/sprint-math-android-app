package com.example.s530671.sprintmath;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Moderate extends AppCompatActivity {
    int timeLimit=21;
    final ModerateModal modModel = ModerateModal.getModerateModal();
    TextView num1TV, num2TV, questionNoTV, operatorTV;
    GridView choicesGV;

    /***************   Nilantha *************/
    int timer = 0;
    CountDownTimer clock;
    TextView timerTV;
    ImageView markIV;
    /**********************/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moderate);

        //Catching the views
        num1TV = findViewById(R.id.num1TV);
        num2TV = findViewById(R.id.num2TV);
        questionNoTV = findViewById(R.id.questionNoTV);
        operatorTV = findViewById(R.id.operationTV);
        choicesGV = findViewById(R.id.choicesGV);
/***********        nilantha ***************/
        timerTV = findViewById(R.id.timerTV);
        markIV = findViewById(R.id.markIV);
/*****************************************/
        //setting the TextViews
        num1TV.setText(modModel.getNewNum1());
        operatorTV.setText(modModel.getNewOperator());
        num2TV.setText(modModel.getNewNum2());
        questionNoTV.setText(String.valueOf(modModel.getQuestionNo()));

        clock = new CountDownTimer(1000 * 1200, 1000) {
            public void onTick(long secondsUntilFinished) {
                if (timer < timeLimit) {
                    timerTV.setText(String.format("%02d", timer / 60) + ":" + String.format("%02d", timer % 60));
                    timer++;
                }
                else {
                    clock.cancel();
                    Toast.makeText(Moderate.this, "Timer over", Toast.LENGTH_SHORT).show();
                    Intent modIntent = new Intent(getApplicationContext(),ModSummary.class);
                    finish();
                    startActivity(modIntent);
                }
            }
            public void onFinish() {
            }
        }.start();

        ListAdapter modListApadter = new ArrayAdapter<Integer>(this, R.layout.choice_option, R.id.choiceTV, modModel.getChoices()){

        };
        choicesGV.setAdapter(modListApadter);

        choicesGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                /***************   Nilantha *************/
                if (timer < timeLimit) {
                    int selected = modModel.choices.get(i);
                    /********Nilantha************/
                    modModel.setSelectedAnswer(selected);
                    int answer = modModel.getCorrectAnswer();
                    if (selected == answer) {
                        markIV.setImageResource(R.drawable.smiley);
                    } else {
                        markIV.setImageResource(R.drawable.sad);
                    }
                    num1TV.setText(modModel.getNewNum1());
                    operatorTV.setText(modModel.getNewOperator());
                    num2TV.setText(modModel.getNewNum2());
                    questionNoTV.setText(String.valueOf(modModel.getQuestionNo()));
                    choicesGV.invalidateViews();
                }
            }

        });
    }

    public void onBackPressed() {
        super.onBackPressed();
        modModel.num1.clear();
        modModel.num2.clear();
        modModel.answer.clear();
        modModel.operator.clear();
        modModel.selectedAnswer.clear();
        modModel.questionNo = 0;
        modModel.choices.clear();
        clock.cancel();
        finish();
    }

}
