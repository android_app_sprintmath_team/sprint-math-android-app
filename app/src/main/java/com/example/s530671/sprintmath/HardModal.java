package com.example.s530671.sprintmath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Chilka on 3/9/2018.
 */

public class HardModal {
    ArrayList<Integer> num1 = new ArrayList<>();
    ArrayList<Integer> num2 = new ArrayList<>();
    ArrayList<Integer> answer = new ArrayList<>();
    ArrayList<Integer> selectedAnswer = new ArrayList<>();
    ArrayList<Integer> choices = new ArrayList<>();
    ArrayList<String> operator = new ArrayList<>();
    int questionNo = 0;
    int currentScore = 0;
    int bestScore = 0;


    Random rand = new Random();

    private static HardModal hardModal = null;

    //    Constructor
    private HardModal(){

    }

    public static HardModal getHardModal(){
        if(hardModal == null){
            hardModal = new HardModal();
        }
        return hardModal;
    }

    public static void setHardModal(HardModal hardModal) {
        HardModal.hardModal = hardModal;
    }

    public ArrayList<Integer> getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1.add(num1);
    }

    public ArrayList<Integer> getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2.add(num2);
    }

    public ArrayList<Integer> getAnswer() {
        return answer;
    }

    public void setAnswer(int num){
        this.answer.add(num);
    }

    public ArrayList<Integer> getSelectedAnswer() {
        return selectedAnswer;
    }

    public void setSelectedAnswer(int selectedAnswer) {
        this.selectedAnswer.add(selectedAnswer);
    }

    public ArrayList<Integer> getChoices() {
        return choices;
    }

    public void setChoices(int answer) {
        this.choices.clear();
        this.choices.add(answer);
        int temp;
        for (int i = 1; i <4; i++) {
            temp = answer+ rand.nextInt(10) - 3;
            if(choices.contains(temp) || temp <0){
                i--;
            }
            else{
                choices.add(temp);
            }
        }
        Collections.shuffle(choices);
    }

    public ArrayList<String> getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator.add(operator);
    }

    public int getQuestionNo() {
        return questionNo;
    }

    public void setQuestionNo(int questionNo) {
        this.questionNo = questionNo;
    }

    public String getNewNum1(){
        int number = rand.nextInt(20);
        number += 20;
        this.setNum1(number);
        questionNo++;
        return String.valueOf(number);
    }

    public String getNewOperator(){
        boolean b = rand.nextBoolean();

        if (b) {
            this.setOperator("+");
            return "+";
        }
        else {
            this.setOperator("-");
            return "-";
        }
    }

    public String getNewNum2(){
        int number = rand.nextInt(20);
        this.setNum2(number);
        this.setNewAnswer();
        return String.valueOf(number);
    }

    public void setNewAnswer() {
        int num1 = this.getNum1().get(questionNo-1);
        int num2 = this.getNum2().get(questionNo-1);
        String op = this.getOperator().get(questionNo-1);

        if (op.equals("+")){
            this.setAnswer(num1 + num2);
            this.setChoices(num1 + num2);
        }
        else{
            this.setAnswer(num1 - num2);
            this.setChoices(num1 - num2);
        }
    }

    public int getCorrectAnswer(){
        return answer.get(answer.size()-1);
    }

    public int getNumberOfCorrects(){
        int corrects=0;

        for(int i = 0; i<selectedAnswer.size(); i++){
            if(answer.get(i) == selectedAnswer.get(i)){
                corrects++;
            }
        }

        return corrects;
    }

    public int getCurrentScore(){
        this.currentScore =((selectedAnswer.size())-(selectedAnswer.size()-getNumberOfCorrects()))*6;
        return currentScore;
    }
    public int getBestScore() {
        return bestScore;
    }

    public void setBestScore(int bestScore) {
//        if (this.bestScore <= currentScore) {
            this.bestScore = bestScore;
//        }
    }

}

