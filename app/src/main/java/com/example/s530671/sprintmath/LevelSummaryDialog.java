package com.example.s530671.sprintmath;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

public class LevelSummaryDialog extends DialogFragment {
    String level;int bestscore=0;int currentscore=0;
//    EasyModal model;
    public LevelSummaryDialog() {
    }

    ClearHistory cHistory;  //reference to the interface

    //Interface to access the activity
    public interface ClearHistory{
        void clearHistory();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        cHistory = (ClearHistory) context;
    }

    @SuppressLint("ValidFragment")
    public LevelSummaryDialog(String level) {
        this.level = level;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        android.support.v7.app.AlertDialog.Builder builder =
                new android.support.v7.app.AlertDialog.Builder(getActivity());
        if(level.equalsIgnoreCase("Moderate")){
            final ModerateModal    model    = ModerateModal.getModerateModal();
            bestscore=model.getBestScore();
            currentscore=model.getCurrentScore();
            builder.setNeutralButton("Clear History", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    model.setBestScore(0);
                    cHistory.clearHistory();
                }
            });

        }
        else if(level.equalsIgnoreCase("Hard")){
            final HardModal    model    = HardModal.getHardModal();
            bestscore=model.getBestScore();
            currentscore=model.getCurrentScore();
            builder.setNeutralButton("Clear History", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    model.setBestScore(0);
                    cHistory.clearHistory();
                }
            });

        }
        else if(level.equalsIgnoreCase("Easy")){
            final EasyModal    model    = EasyModal.getEasyModal();
            bestscore=model.getBestScore();
            currentscore=model.getCurrentScore();
            builder.setNeutralButton("Clear History", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    model.setBestScore(0);
                    cHistory.clearHistory();
                }
            });

        }
        builder.setTitle("Level "+level+" Summary").setMessage("Level Best Score : " +
                bestscore + "\nCurrent Score : " + currentscore);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return builder.create();
    }
}
