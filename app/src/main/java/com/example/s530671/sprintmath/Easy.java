package com.example.s530671.sprintmath;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

public class Easy extends AppCompatActivity {
    final EasyModal easyModal = EasyModal.getEasyModal();
    GridView choicesGV;
    TextView num1TV, num2TV, questionNoTV, timerTV, operatorTV;
    ImageView markIV;
    int timer=0;
    CountDownTimer clock;
    int time;
    int numberOfQuestions = 20;
    boolean summary = false;

    boolean timerOn = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_easy);
        timerTV = findViewById(R.id.timerTV);
        choicesGV = findViewById(R.id.choicesGV);
        num1TV = findViewById(R.id.num1TV);
        operatorTV = findViewById(R.id.operationTV);
        num2TV = findViewById(R.id.num2TV);
        questionNoTV = findViewById(R.id.questionNoTV);
        markIV = findViewById(R.id.markIV);
        num1TV.setText(easyModal.getNewNumberOne());
        operatorTV.setText(easyModal.getNewOperator());
        num2TV.setText(easyModal.getNewNumberTwo());
        questionNoTV.setText(easyModal.getQuestionNo());

        clock = new CountDownTimer(1000 * 1200, 1000) {

            public void onTick(long secondsUntilFinished) {
                timerTV.setText(String.format("%02d", timer/60) + ":" + String.format("%02d",timer%60));
                timer++;
            }
            public void onFinish() {
            }
        }.start();

        ListAdapter choicesAdapter = new ArrayAdapter<Integer>(this, R.layout.choice_option, R.id.choiceTV, easyModal.getChoices()) {

        };
        choicesGV.setAdapter(choicesAdapter);

        choicesGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (Integer.parseInt(easyModal.getQuestionNo()) < numberOfQuestions+1) {
                    int selected = easyModal.choices.get(i);
                    easyModal.setSelectedAnswer(selected);
                    int answer = easyModal.getCorrectAnswer();
                    if (selected == answer) {
                        markIV.setImageResource(R.drawable.smiley);
                    } else {
                        markIV.setImageResource(R.drawable.sad);
                    }
                    if (Integer.parseInt(easyModal.getQuestionNo()) < numberOfQuestions) {
                        num1TV.setText(easyModal.getNewNumberOne());
                        operatorTV.setText(easyModal.getNewOperator());
                        num2TV.setText(easyModal.getNewNumberTwo());
                        questionNoTV.setText(easyModal.getQuestionNo());
                        choicesGV.invalidateViews();
                    }
                    else {
                        if(summary == false) {
                            summary = true;
                            clock.cancel();
                            time = timer;
                            easyModal.setCurrentTime(time);
                            Intent summaryIntent = new Intent(getApplicationContext(), EasySummary.class);
                            finish();
                            startActivity(summaryIntent);
                        }
                    }
                }
            }

        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        easyModal.num1.clear();
        easyModal.num2.clear();
        easyModal.answer.clear();
        easyModal.operator.clear();
        easyModal.selectedAnswer.clear();
        easyModal.questionNo = 0;
        easyModal.choices.clear();
        clock.cancel();
    }
}
