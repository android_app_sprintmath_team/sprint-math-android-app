package com.example.s530671.sprintmath;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ModSummary extends AppCompatActivity implements LevelSummaryDialog.ClearHistory{

    private ModerateModal modModel = ModerateModal.getModerateModal();
    TextView resultTV, scoreTV;
    ListView summaryLV;
    static String BESTSCORE = "BestScore";
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_summary);
        resultTV = findViewById(R.id.resultTV);
        int noOfCorrects = modModel.getNumberOfCorrects();
        resultTV.setText("You got " + noOfCorrects + "/"+(modModel.selectedAnswer.size()>0?modModel.selectedAnswer.size():1));
        summaryLV = findViewById(R.id.summaryLV);
        sp = getPreferences(0);
        modModel.setBestScore(sp.getInt(BESTSCORE, 0));


        if(modModel.getCurrentScore()> modModel.getBestScore() ){
            BestScoreDialog bestScoreDialog = new BestScoreDialog("Moderate");
            bestScoreDialog.show(getSupportFragmentManager(), null);
            bestScoreDialog.setCancelable(false);
            modModel.setBestScore(modModel.getCurrentScore());
            SharedPreferences.Editor edit = sp.edit();
            edit.putInt(BESTSCORE, modModel.getBestScore());
            edit.commit();

        }
        if(modModel.getCurrentScore()>=60){
            CongratsDialog congratsDialog = new CongratsDialog();
            congratsDialog.show(getFragmentManager(), null);
            congratsDialog.setCancelable(false);
        }


        scoreTV = findViewById(R.id.scoreTV);
        scoreTV.setText("SCORE : " + modModel.getCurrentScore());

        ListAdapter summaryAdapter = new ArrayAdapter<Integer>(this, R.layout.summary_row, R.id.correctAnswerTV, modModel.answer){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                TextView questionNoTV = v.findViewById(R.id.questionNoTV);
                TextView questionTV = v.findViewById(R.id.questionTV);
                TextView selectedTV = v.findViewById(R.id.givenAnswerTV);
                TextView answerTV = v.findViewById(R.id.correctAnswerTV);
                int selectedAnswer=-1;
                if(position<modModel.selectedAnswer.size())
                {
                    selectedAnswer = modModel.selectedAnswer.get(position);
                }
                int answer = modModel.answer.get(position);
                questionNoTV.setText(String.valueOf(position + 1)+ ".");
                String question = modModel.num1.get(position) + modModel.operator.get(position) + modModel.num2.get(position);
                questionTV.setText(question + " = " );

                if(answer != selectedAnswer){
                    selectedTV.setText(String.valueOf(selectedAnswer>-1?selectedAnswer:"none"));
                    selectedTV.setTextColor(Color.parseColor("#FF0000"));
                }
                else {
                    selectedTV.setTextColor(Color.parseColor("#FFFFFF"));
                    selectedTV.setText(String.valueOf(selectedAnswer));
                }
                return  v;
            }
        };
        summaryLV.setAdapter(summaryAdapter);
    }

    public void restart (View v){
        modModel.num1.clear();
        modModel.num2.clear();
        modModel.answer.clear();
        modModel.selectedAnswer.clear();
        modModel.questionNo = 0;
        modModel.choices.clear();
        modModel.operator.clear();
        Intent modIntent = new Intent(this, Moderate.class);
        finish();
        startActivity(modIntent);
    }

    public void quit(View v){
        Intent quitIntent = new Intent(getApplicationContext(), DifficultyLevel.class);
        quitIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(quitIntent);
        System.exit(0);
    }

    public void levelSummary(View v){
        LevelSummaryDialog eLevelSummary = new LevelSummaryDialog("Moderate");
        eLevelSummary.show(getSupportFragmentManager(), null);
        eLevelSummary.setCancelable(false);
    }
    @Override

    public void clearHistory() {
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(BESTSCORE, modModel.getBestScore());
        edit.commit();
    }


}
