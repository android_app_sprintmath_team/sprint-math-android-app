package com.example.s530671.sprintmath;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class EasySummary extends AppCompatActivity implements LevelSummaryDialog.ClearHistory{
    private EasyModal easyModal = EasyModal.getEasyModal();
    TextView resultTV, scoreTV;
    ListView summaryLV;
    static String BESTSCORE = "BestScore";
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_easy_summary);
        resultTV = findViewById(R.id.resultTV);
        int noOfCorrects = easyModal.getNumberOfCorrects();
        resultTV.setText("You got " + noOfCorrects + "/" +( easyModal.answer.size()));
        summaryLV = findViewById(R.id.summaryLV);

        sp = getPreferences(0);
        easyModal.setBestScore(sp.getInt(BESTSCORE, 0));


        if(easyModal.getCurrentScore()> easyModal.getBestScore() ){
            BestScoreDialog bestScoreDialog = new BestScoreDialog("Easy");
            bestScoreDialog.show(getSupportFragmentManager(), null);
            bestScoreDialog.setCancelable(false);
            easyModal.setBestScore(easyModal.getCurrentScore());
            SharedPreferences.Editor edit = sp.edit();
            edit.putInt(BESTSCORE, easyModal.getBestScore());
            edit.commit();

        }
        if(easyModal.getCurrentScore()>=150){
            CongratsDialog congratsDialog = new CongratsDialog();
            congratsDialog.show(getFragmentManager(), null);
            congratsDialog.setCancelable(false);
        }
        scoreTV = findViewById(R.id.scoreTV);
        scoreTV.setText("SCORE : " + easyModal.getCurrentScore());

        ListAdapter summaryAdapter = new ArrayAdapter<Integer>(this, R.layout.summary_row, R.id.correctAnswerTV, easyModal.answer){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                    TextView questionNoTV = v.findViewById(R.id.questionNoTV);
                    TextView questionTV = v.findViewById(R.id.questionTV);
                    TextView selectedTV = v.findViewById(R.id.givenAnswerTV);
                    TextView answerTV = v.findViewById(R.id.correctAnswerTV);
                    int answer = easyModal.answer.get(position);
                    int selectedAnswer = easyModal.selectedAnswer.get(position);
                    questionNoTV.setText(String.valueOf(position + 1)+ ".");
                    String question = easyModal.num1.get(position) + easyModal.operator.get(position) + easyModal.num2.get(position);
                    questionTV.setText(question + " = " );

                    if(answer != selectedAnswer){
                        selectedTV.setText(String.valueOf(selectedAnswer));
                        selectedTV.setTextColor(Color.parseColor("#FF0000"));
                    }
                    else {
                        selectedTV.setTextColor(Color.parseColor("#FFFFFF"));
                        selectedTV.setText(String.valueOf(selectedAnswer));
                    }
                return  v;
            }
        };
        summaryLV.setAdapter(summaryAdapter);


    }

    public void restart (View v){
        easyModal.num1.clear();
        easyModal.num2.clear();
        easyModal.answer.clear();
        easyModal.operator.clear();
        easyModal.selectedAnswer.clear();
        easyModal.questionNo = 0;
        easyModal.choices.clear();
        Intent easyIntent = new Intent(this, Easy.class);
        finish();
        startActivity(easyIntent);
    }

    public void quit(View v){
        Intent quitIntent = new Intent(getApplicationContext(), DifficultyLevel.class);
        quitIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(quitIntent);
        System.exit(0);
    }

    public void levelSummary(View v){
        LevelSummaryDialog eLevelSummary = new LevelSummaryDialog("Easy");
        eLevelSummary.show(getSupportFragmentManager(), null);
        eLevelSummary.setCancelable(false);
    }

    @Override
    public void clearHistory() {
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(BESTSCORE, easyModal.getBestScore());
        edit.commit();
    }
}
