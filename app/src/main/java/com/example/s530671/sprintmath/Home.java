package com.example.s530671.sprintmath;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import static android.app.PendingIntent.getActivity;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    public void start (View v){
        Intent startIntent = new Intent(this, DifficultyLevel.class);
        startActivity(startIntent);
    }

    public void quit (View v){
        finish();
        System.exit(0);
    }

}
