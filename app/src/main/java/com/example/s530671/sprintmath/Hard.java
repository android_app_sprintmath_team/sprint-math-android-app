package com.example.s530671.sprintmath;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class Hard extends AppCompatActivity {
    int timeLimit=21;
    final HardModal hModel = HardModal.getHardModal();
    TextView num1TV, num2TV, questionNoTV, operatorTV;
    GridView choicesGV;

    int timer = 0;
    CountDownTimer clock;
    TextView timerTV;
    ImageView markIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hard);

        //Catching the views
        num1TV = findViewById(R.id.num1TV);
        num2TV = findViewById(R.id.num2TV);
        questionNoTV = findViewById(R.id.questionNoTV);
        operatorTV = findViewById(R.id.operationTV);
        choicesGV = findViewById(R.id.choicesGV);
        timerTV = findViewById(R.id.timerTV);
        markIV = findViewById(R.id.markIV);


        //setting the TextViews
        num1TV.setText(hModel.getNewNum1());
        operatorTV.setText(hModel.getNewOperator());
        num2TV.setText(hModel.getNewNum2());
        questionNoTV.setText(String.valueOf(hModel.getQuestionNo()));

        clock = new CountDownTimer(1000 * 1200, 1000) {
            public void onTick(long secondsUntilFinished) {
                if (timer < timeLimit) {
                    timerTV.setText(String.format("%02d", timer / 60) + ":" + String.format("%02d", timer % 60));
                    timer++;
                }
                else {
                    clock.cancel();
                    Toast.makeText(Hard.this, "Timer over", Toast.LENGTH_SHORT).show();
                    Intent hardIntent = new Intent(getApplicationContext(),HardSummary.class);
                    finish();
                    startActivity(hardIntent);
                }
            }
            public void onFinish() {
            }
        }.start();


        ListAdapter hListApadter = new ArrayAdapter<Integer>(this, R.layout.choice_option, R.id.choiceTV, hModel.getChoices()){

        };
        choicesGV.setAdapter(hListApadter);

        choicesGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (timer < timeLimit) {
                    int selected = hModel.choices.get(i);
                    hModel.setSelectedAnswer(selected);
                    int answer = hModel.getCorrectAnswer();

                    //To be implemented Later for smiley face

                    if (selected == answer) {
                        markIV.setImageResource(R.drawable.smiley);
                    } else {
                        markIV.setImageResource(R.drawable.sad);
                    }


                    num1TV.setText(hModel.getNewNum1());
                    operatorTV.setText(hModel.getNewOperator());
                    num2TV.setText(hModel.getNewNum2());
                    questionNoTV.setText(String.valueOf(hModel.getQuestionNo()));
                    choicesGV.invalidateViews();
                }
            }

        });
    }

    public void onBackPressed() {
        super.onBackPressed();
        hModel.num1.clear();
        hModel.num2.clear();
        hModel.answer.clear();
        hModel.operator.clear();
        hModel.selectedAnswer.clear();
        hModel.questionNo = 0;
        hModel.choices.clear();
        clock.cancel();
        finish();
    }
}
