package com.example.s530671.sprintmath;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class BestScoreDialog extends DialogFragment{
    String level;int bestscore=0;int currentscore=0;

    public BestScoreDialog() {
    }
    @SuppressLint("ValidFragment")
    public BestScoreDialog(String a) {
        level=a;
        if(level.equals("Easy")){
            EasyModal model    = EasyModal.getEasyModal();
            bestscore=model.getBestScore();
            currentscore=model.getCurrentScore();
        }
        else if(level.equals("Moderate")){
            ModerateModal    model    = ModerateModal.getModerateModal();
            bestscore=model.getBestScore();
            currentscore=model.getCurrentScore();

        }
        else if(level.equals("Hard")){
            HardModal  model    = HardModal.getHardModal();
            bestscore=model.getBestScore();
            currentscore=model.getCurrentScore();
        }

    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Congrats..!, Best score of the level"). setMessage("Current Score : " + currentscore+ "\nPrevious Best Score : " + bestscore).
                setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        return  builder.create();
    }
}
