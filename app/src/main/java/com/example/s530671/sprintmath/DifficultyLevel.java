package com.example.s530671.sprintmath;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DifficultyLevel extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_difficulty_level);
    }

    public void goEasy(View v){
        Intent easyIntent = new Intent(this, Easy.class);
        startActivity(easyIntent);

    }

    public void goModerate(View v){
        Intent moderateIntent = new Intent(this, Moderate.class);
        startActivity(moderateIntent);
    }

    public void goHard(View v){
        Intent hardIntent = new Intent(this, Hard.class);
        startActivity(hardIntent);
    }
}
