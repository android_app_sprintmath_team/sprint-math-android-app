package com.example.s530671.sprintmath;

import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by S528116 on 3/8/2018.
 */

public class EasyModal {
    private static EasyModal easyModal = null;
    ArrayList<Integer> num1 = new ArrayList<>();
    ArrayList<Integer> num2 = new ArrayList<>();
    ArrayList<String> operator = new ArrayList<>();
    ArrayList<Integer> answer = new ArrayList<>();
    ArrayList<Integer> selectedAnswer = new ArrayList<>();
    ArrayList<Integer> choices = new ArrayList<>();
    int questionNo = 0;
    int currentTime=0;
    int bestTime = 0;
    int currentScore = 0;
    int bestScore = 0;


    Random rand = new Random();

//    Constructor
    private EasyModal(){}

    public static EasyModal getEasyModal(){
        if(easyModal == null){
            easyModal = new EasyModal();
        }

        return easyModal;
    }

    public int getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(int currentTime) {
        this.currentTime = currentTime;
        setCurrentScore(currentTime);
    }

    public int getBestTime() {
        return bestTime;
    }

    public void setBestTime(int bestTime) {
        this.bestTime = bestTime;
    }

//    Get first number for the equation
    public String getNewNumberOne (){
        int num = rand.nextInt(4) +6;
        questionNo++;
        setNum1(num);   // add the number to the list
        return String.valueOf(num);
    }

//    Get second number for the equation
    public String getNewNumberTwo(){
        int num = rand.nextInt(6);
        setNum2(num);
        setAnswer();
        return String.valueOf(num);
    }
    public ArrayList<String> getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator.add(operator);
    }

    public String getNewOperator(){
        boolean b = rand.nextBoolean();

        if (b) {
            this.setOperator("+");
            return "+";
        }
        else {
            this.setOperator("-");
            return "-";
        }
    }
    public ArrayList<Integer> getNum1() {
        return num1;
    }

    public void setNum1(int num) {
        this.num1.add(num);
    }

    public ArrayList<Integer> getNum2() {
        return num2;
    }

    public void setNum2(int num) {
        this.num2.add(num);
    }

    public ArrayList<Integer> getAnswer() {
        return answer;
    }

    public int getCorrectAnswer(){
        return answer.get(answer.size()-1);
    }

    public void setAnswer() {
        int tempanswer;
        int num1 = this.num1.get(questionNo-1);
        int num2 = this.num2.get(questionNo-1);
        String op = this.getOperator().get(questionNo-1);

        if (op.equals("+")){
            tempanswer = num1+num2;
            this.answer.add(tempanswer);
            this.setChoices(tempanswer);
        }
        else{
            tempanswer = num1 - num2;
            this.answer.add(tempanswer);
            this.setChoices(tempanswer);
        }
    }

    public ArrayList<Integer> getCorrectAnswerList(){
        return answer;
    }
    public ArrayList<Integer> getSelectedAnswer() {
        return selectedAnswer;
    }

    public void setSelectedAnswer(int selectedAnswer) {
        this.selectedAnswer.add(selectedAnswer);
    }

    public ArrayList<Integer> getChoices() {
        return choices;
    }

    public void setChoices(int answer) {
        choices.clear();
        this.choices.add(answer);
        int temp;
        for (int i = 1; i <4; i++) {
            temp = answer+ rand.nextInt(10) - 3;
            if(choices.contains(temp) || temp <0){
                i--;
            }
            else{
                choices.add(temp);
            }
        }
        Collections.shuffle(choices);
    }

    public String getQuestionNo(){
        return String.valueOf(this.questionNo);
    }

    public int getNumberOfCorrects(){
        int corrects=0;
        for(int i = 0; i<answer.size(); i++){
            if(answer.get(i) == selectedAnswer.get(i)){
                corrects++;
            }
        }

        return corrects;
    }

    public int getCurrentScore(){
        return currentScore;
    }

    public void setCurrentScore(int currentTime){
        this.currentScore =200-( currentTime + (20-getNumberOfCorrects())*5);

    }

    public int getBestScore(){
        return this.bestScore;
    }

    public void setBestScore(int currentScore){
//        if(this.bestScore<= currentScore){
            this.bestScore = currentScore;
//            savePreferences();
//        }
    }

//
}
