package com.example.s530671.sprintmath;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class HardSummary extends AppCompatActivity implements LevelSummaryDialog.ClearHistory{

    private HardModal hardModel = HardModal.getHardModal();
    TextView resultTV, scoreTV;
    ListView summaryLV;
    static String BESTSCORE = "BestScore";
    SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hard_summary);
        resultTV = findViewById(R.id.resultTV);
        int noOfCorrects = hardModel.getNumberOfCorrects();
        resultTV.setText("You got " + noOfCorrects + "/"+(hardModel.selectedAnswer.size()>0?hardModel.selectedAnswer.size():1));
        summaryLV = findViewById(R.id.summaryLV);

                sp = getPreferences(0);
        hardModel.setBestScore(sp.getInt(BESTSCORE, 0));


        if(hardModel.getCurrentScore()> hardModel.getBestScore() ){
            BestScoreDialog bestScoreDialog = new BestScoreDialog("Hard");
            bestScoreDialog.show(getSupportFragmentManager(), null);
            bestScoreDialog.setCancelable(false);
            hardModel.setBestScore(hardModel.getCurrentScore());
            SharedPreferences.Editor edit = sp.edit();
            edit.putInt(BESTSCORE, hardModel.getBestScore());
            edit.commit();

        }
        if(hardModel.getCurrentScore()>=60){
            CongratsDialog congratsDialog = new CongratsDialog();
            congratsDialog.show(getFragmentManager(), null);
            congratsDialog.setCancelable(false);
        }


        scoreTV = findViewById(R.id.scoreTV);
        scoreTV.setText("SCORE : " + hardModel.getCurrentScore());

        ListAdapter summaryAdapter = new ArrayAdapter<Integer>(this, R.layout.summary_row, R.id.correctAnswerTV, hardModel.answer){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                TextView questionNoTV = v.findViewById(R.id.questionNoTV);
                TextView questionTV = v.findViewById(R.id.questionTV);
                TextView selectedTV = v.findViewById(R.id.givenAnswerTV);
                TextView answerTV = v.findViewById(R.id.correctAnswerTV);
                int selectedAnswer=-1;
                if(position<hardModel.selectedAnswer.size())
                {
                    selectedAnswer = hardModel.selectedAnswer.get(position);
                }
                int answer = hardModel.answer.get(position);
                questionNoTV.setText(String.valueOf(position + 1)+ ".");
                String question = hardModel.num1.get(position) + hardModel.operator.get(position) + hardModel.num2.get(position);
                questionTV.setText(question + " = " );

                if(answer != selectedAnswer){
                    selectedTV.setText(String.valueOf(selectedAnswer>-1?selectedAnswer:"none"));
                    selectedTV.setTextColor(Color.parseColor("#FF0000"));
                }
                else {
                    selectedTV.setTextColor(Color.parseColor("#FFFFFF"));
                    selectedTV.setText(String.valueOf(selectedAnswer));
                }
                return  v;
            }
        };
        summaryLV.setAdapter(summaryAdapter);
    }

    public void restart (View v){
        hardModel.num1.clear();
        hardModel.num2.clear();
        hardModel.answer.clear();
        hardModel.selectedAnswer.clear();
        hardModel.questionNo = 0;
        hardModel.choices.clear();
        hardModel.operator.clear();
        Intent hardIntent = new Intent(this, Hard.class);
        finish();
        startActivity(hardIntent);
    }


    public void quit(View v){
        Intent quitIntent = new Intent(getApplicationContext(), DifficultyLevel.class);
        quitIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(quitIntent);
        System.exit(0);
    }

    public void levelSummary(View v){
        LevelSummaryDialog eLevelSummary = new LevelSummaryDialog("Hard");
        eLevelSummary.show(getSupportFragmentManager(), null);
        eLevelSummary.setCancelable(false);
    }

    @Override
    public void clearHistory() {
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(BESTSCORE, hardModel.getBestScore());
        edit.commit();
    }


}
